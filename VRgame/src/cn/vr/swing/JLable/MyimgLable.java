package cn.vr.swing.JLable;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class MyimgLable extends JLabel {
     private String imgpath;
     
	public String getImgpath() {
		return imgpath;
	}

	public void setImgpath(String imgpath) {
		this.imgpath = imgpath;
	}

	public MyimgLable(String path) {
		super(path);
		// TODO Auto-generated constructor stub
		this.imgpath=path;
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);
		
		ImageIcon icon = new ImageIcon(System.getProperty("user.dir") +imgpath);
        Image img = icon.getImage();
        g.drawImage(img, 0, 0,getWidth(),getHeight(), this); 
	}



}
