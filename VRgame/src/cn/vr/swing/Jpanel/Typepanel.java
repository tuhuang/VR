package cn.vr.swing.Jpanel;
/*
 * author:黄龙
 * time:2016.7.29
 */
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicButtonUI;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.jb2011.lnf.beautyeye.resources.beautyeye;

import cn.vr.swing.JButton.MyimgJbutton;
import cn.vr.swing.Jdialog.newLoginDialog;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Color;

public class Typepanel extends JPanel {
	
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JLabel jLabel7;
	private  MyimgJbutton login;
	private JLabel blank;
	public Typepanel() {
		setLayout(new GridBagLayout());
		GridBagConstraints constraints=new GridBagConstraints();
	    init();
	    
		//第一列
		constraints.gridy=0;
		constraints.gridx=0;
		constraints.gridwidth=1;
		constraints.weightx=10;
		constraints.weighty=100;
		add(blank,constraints);
		
		//第二列
		constraints.gridy=0;
		constraints.gridx=1;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel1,constraints);
		
		//第三列
		constraints.gridy=0;
		constraints.gridx=2;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel2,constraints);
		
		//第四列
		constraints.gridy=0;
		constraints.gridx=3;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel3,constraints);
		
		//第五列
		constraints.gridy=0;
		constraints.gridx=4;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel4,constraints);
		
		//第六列
		constraints.gridy=0;
		constraints.gridx=5;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel5,constraints);
		
		//第七列
		constraints.gridy=0;
		constraints.gridx=6;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel6,constraints);
		
		//第八列
		constraints.gridy=0;
		constraints.gridx=7;
		constraints.gridwidth=1;
		constraints.weightx=11;
		constraints.weighty=100;
		constraints.fill=constraints.NONE;
		add(jLabel7,constraints);
		
		//第九列
		constraints.gridy=0;
		constraints.gridx=8;
		constraints.gridwidth=1;
		constraints.weightx=13;
		constraints.weighty=100;
		constraints.ipadx=30;
		constraints.ipady=30;
		constraints.fill=constraints.NORTH;
		add(login,constraints);
		
	    setOpaque(false);
		
	}
	
	public void init()
	{
		jLabel1=new JLabel("动作类");
		jLabel2=new JLabel("冒险类");
		jLabel3=new JLabel("策略类");
		jLabel4=new JLabel("体育类");
		jLabel5=new JLabel("竞速类");
		jLabel6=new JLabel("射击类");
		jLabel7=new JLabel("其他类");
		login=new MyimgJbutton("pic/login.png");
		login.setUI(new BasicButtonUI());// 恢复基本视觉效果
		blank=new JLabel();
		blank.setOpaque(false);
		Font font=new Font("宋体", Font.BOLD	, 30);
		jLabel1.setFont(font);
		jLabel2.setFont(font);
		jLabel3.setFont(font);
		jLabel4.setFont(font);
		jLabel5.setFont(font);
		jLabel6.setFont(font);
		jLabel7.setFont(font);
		
		/**
		 * 字体颜色
		 */
		jLabel1.setForeground(Color.WHITE);
		jLabel2.setForeground(Color.WHITE);
		jLabel3.setForeground(Color.WHITE);
		jLabel4.setForeground(Color.WHITE);
		jLabel5.setForeground(Color.WHITE);
		jLabel6.setForeground(Color.WHITE);
		jLabel7.setForeground(Color.WHITE);
		
		login.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//jp.createDialog(parentComponent, title)
				
				newLoginDialog ld=new newLoginDialog();
				ld.setLocation(300, 200);
				ld.setSize(500, 300);
				ld.show();
			}
		});
		
	}

	
	public static void main(String[] args){
		
		try {
			BeautyEyeLNFHelper.launchBeautyEyeLNF();
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
		
	}
}
