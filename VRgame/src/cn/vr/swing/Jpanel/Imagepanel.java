package cn.vr.swing.Jpanel;
/*
 * author:黄龙
 * time:2016.7.29
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import cn.vr.swing.JButton.MyimgJbutton;
import cn.vr.swing.JLable.MyimgLable;


public class Imagepanel extends JPanel{

	private MyimgLable jLabel;
	private MyimgJbutton jButton;
	private JTextField field;
	private JTextArea area;
	public Imagepanel() {
		super();
		// TODO Auto-generated constructor stub
		
	setLayout(new GridBagLayout());
		GridBagConstraints constraints=new GridBagConstraints();
		jLabel=new MyimgLable("/pic/u2.png");
	    jButton=new MyimgJbutton(System.getProperty("user.dir") +"/pic/圆角矩形.png");
	  // jButton.setText("开始游戏");
		field=new JTextField();
		area=new JTextArea();
		
		//第一行
		constraints.gridy=0;
		constraints.gridx=0;
		constraints.gridheight=1;
		constraints.weightx=100;
		constraints.weighty=50;
		constraints.fill=constraints.BOTH;
		add(jLabel, constraints);
		
		//第二行
		constraints.gridy=1;
		constraints.gridx=0;
		constraints.gridheight=1;
		constraints.weightx=100;
		constraints.weighty=10;
		constraints.fill=constraints.HORIZONTAL;
		add(field, constraints);
		//field.setOpaque(false);
		
		//第三行
		constraints.gridy=2;
		constraints.gridx=0;
		constraints.weighty=20;
		constraints.fill=constraints.BOTH;
		add(area, constraints);
		//area.setOpaque(false);
		
		//第四行
		constraints.gridy=3;
		constraints.gridx=0;
		constraints.weighty=20;
		constraints.fill=constraints.NONE;
		constraints.anchor=constraints.EAST;
		constraints.ipadx=80;
		constraints.ipady=25;
		add(jButton, constraints);
		setOpaque(false);
	}


}
