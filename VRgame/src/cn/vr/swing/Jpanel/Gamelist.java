package cn.vr.swing.Jpanel;
import java.awt.Component;
/*
 * author:黄龙
 * time:2016.7.29
 */
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Vector;

import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import org.apache.logging.log4j.core.appender.rolling.OnStartupTriggeringPolicy;
import org.springframework.context.ApplicationContext;


import cn.vr.other.Datasearch;
import cn.vr.swing.JCombox.Myjcombox;
import cn.vr.swing.Jdialog.Addgame;

//游戏列表面板
/*
 * author:黄龙
 * time:2016.7.30
 */


public class Gamelist extends JPanel implements ActionListener,ItemListener {

	/**
	 * Create the panel.
	 */
	
	private static ApplicationContext applicationContext;

	
	
	private JLabel  type;
	private  Myjcombox box;
	private  JButton shaixuan;
	private JButton add;
	private  static JTable jTable;
	private JButton berfore;
	private JButton next;
	private JScrollPane jScrollPane=new JScrollPane();
	DefaultTableModel model;
	Vector<String> colnames;
	

	public Gamelist() {
		setLayout(new GridBagLayout());

		type=new JLabel("类型");
        box=new Myjcombox();
        box.addItemListener(this);
        shaixuan=new JButton("刷新");
        add=new JButton("新增游戏");
        berfore=new JButton();
        next=new JButton();
        jTable=new JTable();
        
    	 colnames=new Vector<String>();
		colnames.add("名称");
		colnames.add("类型");
		colnames.add("路径");
		colnames.add("操作");
		Datasearch data=new Datasearch();
			 model=new DefaultTableModel(data.Dataname("动作类"),colnames);
			jTable.setModel(model);
		
        
		 GridBagConstraints constraints=new GridBagConstraints();
		constraints.gridy=0;
		constraints.gridx=0;
		constraints.weightx=10;
		constraints.weighty=20;
		constraints.fill=constraints.HORIZONTAL;
		add(type, constraints);

		constraints.gridy=0;
		constraints.gridx=1;
		constraints.weightx=15;
		constraints.weighty=20;
		constraints.fill=constraints.HORIZONTAL;
		add(box, constraints);
		
		
		constraints.gridy=0;
		constraints.gridx=2;
		constraints.gridwidth=2;
		constraints.weightx=65;
		constraints.weighty=20;
		constraints.fill=constraints.NONE;
		constraints.anchor=constraints.EAST;
		add(shaixuan, constraints);
		shaixuan.addActionListener(this);

		constraints.gridy=0;
		constraints.gridx=4;
		constraints.weightx=10;
		constraints.weighty=20;
		constraints.fill=constraints.HORIZONTAL;
		add(add, constraints);
		add.addActionListener(this);
		
		

		 
		//第二行
			constraints.gridy=1;
			constraints.gridx=0;
			constraints.gridwidth=6;
			constraints.weighty=80;
			constraints.fill=constraints.BOTH;
			
			jTable.getColumnModel().getColumn(3).setCellRenderer(new MybuttonRender());
			jTable.getColumnModel().getColumn(3).setCellEditor(new MybuttonEditor(jTable,box) );
			
			jScrollPane.setViewportView(jTable);
			add(jScrollPane, constraints);
		

			
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
	
		Datasearch data=new Datasearch();
		if(e.getSource()==shaixuan){
			
		 model=new DefaultTableModel(data.Dataname(box.getSelectedItem().toString()),colnames);
			jTable.setModel(model);
			jTable.getColumnModel().getColumn(3).setCellRenderer(new MybuttonRender());
			jTable.getColumnModel().getColumn(3).setCellEditor(new MybuttonEditor(jTable,box) );
			
		}
		
		if(e.getSource()==add){
			
			Addgame addgame=new Addgame();
			addgame.show();
		}
	}

	@Override
	public void itemStateChanged(ItemEvent e) {
		// TODO Auto-generated method stub
		if (e.getStateChange() == ItemEvent.SELECTED) {
			Datasearch data = new Datasearch();
			model = new DefaultTableModel(data.Dataname(box.getSelectedItem().toString()), colnames);
			jTable.setModel(model);
			jTable.getColumnModel().getColumn(3).setCellRenderer(new MybuttonRender());
			jTable.getColumnModel().getColumn(3).setCellEditor(new MybuttonEditor(jTable,box));
		}
	
	
	}

}
