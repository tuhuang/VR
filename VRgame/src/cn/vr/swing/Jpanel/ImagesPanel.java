package cn.vr.swing.Jpanel;
/*
 * author:黄龙
 * time:2016.7.29
 */
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicButtonUI;

import cn.vr.swing.JButton.MyimgJbutton;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Insets;

public class ImagesPanel extends JPanel {
	
	private MyimgJbutton last;
	private MyimgJbutton next;


	public ImagesPanel() throws HeadlessException {
		
		setLayout(new GridBagLayout());
		
		last=new MyimgJbutton("pic/last.png");
		last.setUI(new BasicButtonUI());// 恢复基本视觉效果
		next=new MyimgJbutton("pic/next.png");
		next.setUI(new BasicButtonUI());// 恢复基本视觉效果
		last.setOpaque(false);
		next.setOpaque(false);
       GridBagConstraints constraints=new GridBagConstraints();
       //第一列
       constraints.gridy=0;
       constraints.gridx=0;
       constraints.weightx=10;
       constraints.weighty=100;
       constraints.gridwidth=1;
       constraints.ipadx=10;
       constraints.ipady=40;
       constraints.fill=constraints.NONE;
       add(last,constraints);
       int i;
		for(i=1;i<5;i++)
		{ 
			constraints.gridy=0;
		       constraints.gridx=i;
		       constraints.weightx=20;
		       constraints.weighty=100;
		       constraints.gridwidth=1;
		       constraints.insets=new Insets(0, 10, 0, 10);
		       constraints.fill=constraints.BOTH;
			Imagepanel im=new Imagepanel();
			add(im,constraints);
		}
		//最后1列
		constraints.gridy=0;
	       constraints.gridx=i;
	       constraints.weightx=10;
	       constraints.weighty=100;
	       constraints.gridwidth=1;
	       constraints.ipadx=10;
	       constraints.ipady=40;
	       constraints.fill=constraints.NONE;
	       add(next,constraints);
		setOpaque(false);
	}

}
