package cn.vr.swing.Jpanel;

/*
 * 
 * author:黄龙
 * time:2016.8.1
 */
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.EventObject;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.EventListenerList;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;

import cn.vr.other.Datasearch;
import cn.vr.po.Information;
import cn.vr.swing.JCombox.Myjcombox;
import cn.vr.swing.Jdialog.*;

public class MybuttonEditor extends JPanel implements TableCellEditor, ActionListener {

	/**
	 * Create the panel.
	 */

	private JButton edit;
	private JButton delete;
	private JTable jTable;

	private EventListenerList listenerList = new EventListenerList();
	// ChangeEvent用于通知感兴趣的参与者事件源中的状态已发生更改。
	private ChangeEvent changeEvent = new ChangeEvent(this);

	
	
	EditorDialog dialog;
	private Myjcombox box;
	public MybuttonEditor(JTable table,Myjcombox box) {
		setLayout(new GridBagLayout());
		edit = new JButton("编辑");
		delete = new JButton("删除");
		jTable = table;
		this.box=box;
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.gridy = 0;
		constraints.gridx = 0;
		constraints.fill = constraints.NONE;
		add(edit, constraints);

		constraints.gridy = 0;
		constraints.gridx = 1;
		constraints.fill = constraints.NONE;
		add(delete, constraints);

		edit.addActionListener(this);
		delete.addActionListener(this);
	}

	@Override
	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isCellEditable(EventObject anEvent) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean shouldSelectCell(EventObject anEvent) {
		// TODO Auto-generated method stub
		return true;
	}

	private void fireEditingStopped() {
		CellEditorListener listener;
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i++) {
			if (listeners[i] == CellEditorListener.class) {
				// 之所以是i+1，是因为一个为CellEditorListener.class（Class对象），
				// 接着的是一个CellEditorListener的实例
				listener = (CellEditorListener) listeners[i + 1];
				// 让changeEvent去通知编辑器已经结束编辑
				// 在editingStopped方法中，JTable调用getCellEditorValue()取回单元格的值，
				// 并且把这个值传递给TableValues(TableModel)的setValueAt()
				listener.editingStopped(changeEvent);
			}
		}
	}

	@Override
	public boolean stopCellEditing() {
		// TODO Auto-generated method stub
		fireEditingStopped();// 请求终止编辑操作从JTable获得
		return true;
	}

	@Override
	public void cancelCellEditing() {
		// TODO Auto-generated method stub

	}

	@Override
	public void addCellEditorListener(CellEditorListener l) {
		// TODO Auto-generated method stub
		listenerList.add(CellEditorListener.class, l);
	}

	@Override
	public void removeCellEditorListener(CellEditorListener l) {
		// TODO Auto-generated method stub
		listenerList.remove(CellEditorListener.class, l);
	}

	@Override
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		// TODO Auto-generated method stub
		return this;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
         //编辑按钮响应事件，实现游戏信息更新
		if (e.getSource() == edit) {

			// JOptionPane.showMessageDialog(null,
			// jTable.getValueAt(jTable.getSelectedRow(), 0));

			int selIndex = jTable.getSelectedRow();
			if (selIndex < 0 || selIndex >= jTable.getRowCount()) {
				return;
			}
			
			
			Vector<String> v = new Vector<String>();
			
			v.add(jTable.getValueAt(selIndex, 0).toString());
			v.add(jTable.getValueAt(selIndex, 1).toString());
		    dialog = new EditorDialog(v,jTable,box);
			dialog.show();
			
		}

		if (e.getSource() == delete) {

			int res;
			res = JOptionPane.showConfirmDialog(null, "确认删除？", "删除", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (res == JOptionPane.YES_OPTION) {
				int selIndex = jTable.getSelectedRow();
				if (selIndex < 0 || selIndex >= jTable.getRowCount()) {
					return;
				}
				Datasearch datasearch = new Datasearch();
				Information information = datasearch.SelectBynameandtype(jTable.getValueAt(selIndex, 0).toString(),
						jTable.getValueAt(selIndex, 1).toString());
				if (!datasearch.deleteInfobyid(information.getInfoId()))
					JOptionPane.showMessageDialog(null, "删除失败");
				else
					((DefaultTableModel) jTable.getModel()).removeRow(selIndex);

			}

		}

	}

}
