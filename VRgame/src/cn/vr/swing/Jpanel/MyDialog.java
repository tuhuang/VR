package cn.vr.swing.Jpanel;



import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;


public class MyDialog extends JDialog {
	private JPanel contentJpanel =new JPanel();
	private JLabel jl1=new JLabel("新增游戏类型");
	private JLabel jl2=new JLabel("游戏类型");
	private JLabel jl3=new JLabel("排    序");
	
	private JTextField jt1=new JTextField(40);
	
	private JTextField jt2=new JTextField(30);
	
	private JButton jb=new JButton("确认");
	
	private Box boxAll=Box.createVerticalBox();
	private Box box1=Box.createHorizontalBox();
	private Box box2=Box.createHorizontalBox();
	private Box box3=Box.createHorizontalBox();
	private Box box4=Box.createHorizontalBox();

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MyDialog() {

		
		//super());
		// TODO Auto-generated constructor stub
		//contentJpanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		//contentJpanel.setLayout(new GridLayout(3,3));
		//jt1.setPreferredSize(new Dimension(140, 21));
		
		box1.add(jl1);
		
		box2.add(jl2);
		box2.add(jt1);
		
		box3.add(jl3); 
		box3.add(jt2);
		
		box4.add(jb);
		
		boxAll.add(box1);
		boxAll.add(box2);
		boxAll.add(box3);
		boxAll.add(box4);
		contentJpanel.add(boxAll);
		this.add(contentJpanel);
		this.setVisible(true);
	}
	
}
