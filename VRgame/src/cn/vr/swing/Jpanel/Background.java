package cn.vr.swing.Jpanel;
/*
 * author:黄龙
 * time:2016.7.29
 */
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class Background extends JPanel {
	  private int width ; 
	    private int height ;
	    private String imgPath ;
	    
	
		

		public Background(int width, int height, String imgPath) {
			super();
			this.width = width;
			this.height = height;
			this.imgPath = imgPath;
		}




		public Background(String imgPath) {
			super();
			this.imgPath = imgPath;
		}




		@Override
		protected void paintComponent(Graphics gs) {
			// TODO Auto-generated method stub
			//Graphics2D g = (Graphics2D) gs;  
	        super.paintComponent(gs);  
	        //
	        ImageIcon icon = new ImageIcon(imgPath);
	        Image img = icon.getImage();
	        gs.drawImage(img, 0, 0,getWidth(),getHeight(), this); 
	      
			
		}  
	    
	
	
}
