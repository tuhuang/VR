package cn.vr.swing.Jpanel;



import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

public class type extends JFrame {

	private JPanel contentPane;
	String[] columnName = { "类型", "排序", "操作"}; 
	 String[][] rowData = { { "张三", "男", }, 
	 { "李四", "男",  }, 
	 }; 
	/**
	 * 整体布局
	 */
	private Box boxAll = Box.createVerticalBox();
	private Box box1 = Box.createHorizontalBox();
	private Box box2 = Box.createVerticalBox();
	private JPanel jp;
	JTable table = new JTable(new DefaultTableModel(rowData, columnName)){
		 public boolean isCellEditable(int row, int column)
         {
             return false;
         }
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		
	};
	// 创建包含表格的滚动窗格
	JScrollPane scrollPane = new JScrollPane(table);
	/**
	 * 布局控件
	 */
	private JButton jb=new JButton("新增游戏类型");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BeautyEyeLNFHelper.launchBeautyEyeLNF();
					type frame = new type();
					frame.setVisible(true);
					frame.setSize(900, 500);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public type() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		contentPane.add(box1, BorderLayout.NORTH);
		box1.add(Box.createHorizontalGlue());
		jb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MyDialog md=new MyDialog();
				md.setLocation(300, 200);
				md.setSize(500, 300);
				md.show();
			}
		});
		box1.add(jb);
		
		table.getColumnModel().getColumn(2).setCellRenderer(new MybuttonRender());
		//table.getColumnModel().getColumn(2).setCellEditor(new MybuttonEditor());
		
		
		contentPane.add(box2,BorderLayout.CENTER);
		box2.add(scrollPane);

	}

}
