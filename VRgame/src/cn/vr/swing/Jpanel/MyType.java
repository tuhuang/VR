/**
 * 
 */
package cn.vr.swing.Jpanel;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

/**
 * @author 王鹏
 *
 */
public class MyType extends JPanel {
	private JPanel contentPane;
	private Box boxAll = Box.createVerticalBox();
	private Box box0=Box.createHorizontalBox();
	private Box box1 = Box.createHorizontalBox();
	private Box box2 = Box.createVerticalBox();
	private JPanel jp;
	private JButton jb=new JButton("新增游戏类型");
	
	
	public MyType(){
		String[] columnName = { "类型", "排序", "操作"}; 
		String[][] rowData = { { "射击类", "1", }, { "益智类", "2",  }, }; 
		
		
		JTable table = new JTable(new DefaultTableModel(rowData, columnName));
		JScrollPane scrollPane = new JScrollPane(table);
		
		
		//setDefaultCloseOperation(JPanel);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		//contentPane.add(box1, BorderLayout.NORTH);
		box1.add(Box.createHorizontalGlue());
		jb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				MyDialog md=new MyDialog();
				md.setLocation(300, 200);
				md.setSize(500, 300);
				md.show();
			}
		});
		
		box0.add(jb);
		boxAll.add(box0);
		boxAll.add(box1);
		boxAll.add(box2);
		contentPane.add(boxAll);
		this.add(contentPane);
		
		table.getColumnModel().getColumn(2).setCellRenderer(new TypeButtionRender());
		table.getColumnModel().getColumn(2).setCellEditor(new TypeButtonEditor());
		
		
		contentPane.add(box2,BorderLayout.CENTER);
		box2.add(scrollPane);
		
		
	}

	private void setContentPane(JPanel contentPane2) {
		// TODO Auto-generated method stub
		
	}
}
