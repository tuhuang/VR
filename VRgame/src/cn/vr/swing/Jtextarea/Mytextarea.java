package cn.vr.swing.Jtextarea;

/*
 * 
 * author:黄龙
 * time:2016.8.1
 */
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.undo.UndoManager;

//自定义jtextarea
public class Mytextarea extends JTextArea implements MouseListener {
 /**
  * 
  */
 private static final long serialVersionUID = 1L;
 private JPopupMenu pm=null;
 private JMenuItem copy=null;
 private JMenuItem delete=null;
 private JMenuItem cut=null;
 private JMenuItem undo=null;
 private JMenuItem paste=null;
 private JMenuItem redo=null;
 private JMenuItem selectAll=null;
 //添加撤消管理器
 private UndoManager um=new UndoManager();
 public Mytextarea()
 {
  super();
  init();
  setLineWrap(true);	
  addKeyListener(new KeyListener() {
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		if(getText().length()>200)
		{
			
			e.setKeyChar('\0');
			
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
});
 }
 public Mytextarea(String str)
 {
  super(str);
  init();
 }
 private void init()
 {
  this.addMouseListener(this);
  this.getDocument().addUndoableEditListener(um);
  pm=new JPopupMenu();
  
  //全选
  selectAll=new JMenuItem("全选");
  selectAll.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  //粘贴
  paste=new JMenuItem("粘贴");
  paste.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  
  //复制
  copy=new JMenuItem("复制");
  copy.addActionListener(new ActionListener(){
  public void actionPerformed(ActionEvent e) {
   action(e);
   }
  });
  
  //删除
  delete=new JMenuItem("删除");
  delete.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  //剪切
  cut=new JMenuItem("剪切");
  cut.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  
  //撤消
  undo=new JMenuItem("撤消");
  undo.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  //返回
  redo=new JMenuItem("返回");
  redo.addActionListener(new ActionListener(){
   public void actionPerformed(ActionEvent e) {
    action(e);
    }
   });
  pm.add(selectAll);
  pm.add(delete);
  pm.add(new JSeparator());
  pm.add(copy);
  pm.add(cut);
  pm.add(paste);
  pm.add(new JSeparator());
  pm.add(undo);
  pm.add(redo);
  this.add(pm);
 }
 //
 public boolean isClipboardString()
 {
  boolean b=false;
  Clipboard clipboard = this.getToolkit().getSystemClipboard();
  Transferable content = clipboard.getContents(this);
  try 
  {
    if(content.getTransferData(DataFlavor.stringFlavor) instanceof String) 
     b=true;
   } 
  catch (Exception e) 
  {}
  return b;
 }
 //
  public boolean isCanCopy() 
  {
  boolean b = false;
  int start = this.getSelectionStart();
  int end = this.getSelectionEnd();
  if (start != end)
      b = true;
  return b;
  }
 @Override
 public void mouseClicked(MouseEvent arg0) {
  // TODO Auto-generated method stub
  
 }

 @Override
 public void mouseEntered(MouseEvent arg0) {
  // TODO Auto-generated method stub
  
 }

 @Override
 public void mouseExited(MouseEvent arg0) {
  // TODO Auto-generated method stub
  
 }

 @Override
 public void mousePressed(MouseEvent e) {
  // TODO Auto-generated method stub
  if(e.getButton() == MouseEvent.BUTTON3)
  {
    delete.setEnabled(isCanCopy());
    copy.setEnabled(isCanCopy());
    paste.setEnabled(isClipboardString());
    cut.setEnabled(isCanCopy());
    undo.setEnabled(um.canUndo());
    redo.setEnabled(um.canRedo());
    pm.show(this, e.getX(), e.getY());
  }
 }

 @Override
 public void mouseReleased(MouseEvent arg0) {
  // TODO Auto-generated method stub
  
 }
 
 public void action(ActionEvent e)
 {
  String str = e.getActionCommand();
  if(str.equals(copy.getText())) 
  { // 复制
   this.copy();
  } 
  else if(str.equals(paste.getText())) 
  { // 粘贴
      this.paste();
   } 
  else if(str.equals(cut.getText())) 
  { // 剪切
    this.cut();
  }
  else if(str.equals(undo.getText()))
  {
   um.undo();
  }
  else if(str.equals(redo.getText()))
  {
   um.redo();
  }
  else if(str.equals(delete.getText()))
  {
   this.replaceSelection("");   
  }
  else if(str.equals(selectAll.getText()))
  {
   this.selectAll();
  }
 }

}