package cn.vr.swing.Jfram;
/*
 * author:黄龙
 * time:2016.8.2
 */
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.RootPaneContainer;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.RootPaneUI;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import cn.vr.swing.Jpanel.*;
 import cn.vr.swing.Jpanel.Typepanel;

public class GameMain extends JFrame implements ActionListener {

	Typepanel type;
	ImagesPanel imagesPanel;
	GridBagConstraints constraints;

	public GameMain() throws HeadlessException {
		super();

		Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) screensize.getWidth();// 得到宽
		int height = (int) screensize.getHeight();// 得到高

		// setLayout(new BorderLayout());
		Background background = new Background("pic/background.png");
		background.setLayout(new GridBagLayout());
		// background.setLayout(new GridLayout(2, 1));
		add(background);
		EmptyBorder emptyBorder = (EmptyBorder) BorderFactory.createEmptyBorder();// 创建空边框
		type = new Typepanel();
		imagesPanel = new ImagesPanel();

		constraints = new GridBagConstraints();
		// 第一行
		constraints.gridy = 0;
		constraints.gridx = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.weightx = 100;
		constraints.weighty = 15;
		constraints.fill = constraints.BOTH;
		background.add(type, constraints);
		// background.add(type);
		// 第二行
		constraints.gridy = 1;
		constraints.gridx = 0;
		constraints.gridheight = 1;
		constraints.gridwidth = 1;
		constraints.weightx = 100;
		constraints.weighty = 85;
		constraints.fill = constraints.BOTH;
		background.add(imagesPanel, constraints);

		

		setExtendedState(JFrame.MAXIMIZED_BOTH);

		// 定位窗口
		// this.setLocation(20, 20);
		// 设置窗口大小
		// this.setSize(400,400);
		setVisible(true);
		setResizable(false);
		
		
		
	}

	public void actionPerformed(ActionEvent e) {

	}

	

	public static void main(String[] args) {

		try {
			// 设置本属性将改变窗口边框样式定义
			BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;

			org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
		} catch (Exception e) {
			// TODO exception
		}

		// TODO Auto-generated method stub
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new GameMain().setVisible(true);
			}
		});
	}

	

}
