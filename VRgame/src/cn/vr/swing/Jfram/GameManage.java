package cn.vr.swing.Jfram;
/*
 * author:黄龙,王鹏
 * time:2016.7.29
 */
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

import cn.vr.swing.Jpanel.Gamelist;
import cn.vr.swing.Jpanel.MyType;

import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JTabbedPane;

public class GameManage extends JFrame {

	private JPanel contentPane;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		 try
		    {
		        //设置本属性将改变窗口边框样式定义
		        BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
		   
		      
		        org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
		    }
		    catch(Exception e)
		    {
		        //TODO exception
		    }
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GameManage frame = new GameManage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GameManage() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		
		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		tabbedPane.setBounds(5, 5, 424, 252);
		getContentPane().add(tabbedPane);
		
		Gamelist panel_1=new Gamelist();
		tabbedPane.addTab("游戏列表", null, panel_1, null);
		
		//JPanel panel = new JPanel();
		MyType mt=new MyType();
		tabbedPane.addTab("游戏类型", null, mt, null);
		
		  // 定位窗口
		   this.setLocation(300, 100);
		    // 设置窗口大小
		    this.setSize(700,500);
			setVisible(true);
	}

}
