package cn.vr.swing.JButton;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class MyimgJbutton extends JButton {

	private String path;

	public MyimgJbutton(String path) {
		super();
		this.path = path;
	}

	public MyimgJbutton() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void paintComponent(Graphics g) {
		// TODO Auto-generated method stub
		super.paintComponent(g);

		ImageIcon icon = new ImageIcon(path);
        Image img = icon.getImage();
        g.drawImage(img, 0, 0,getWidth(),getHeight(), this); 
	}
	
}
