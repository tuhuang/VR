package cn.vr.swing.Jdialog;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.vr.mapper.UserMapper;
import cn.vr.po.User;
import cn.vr.swing.Jfram.GameManage;

public class newLoginDialog extends JDialog {
	private static JPanel contentPane;
	private JTextField textField;
	private JPasswordField password;
	private static JButton button_1;
	private ApplicationContext applicationContext;
	private User users;
	
	public newLoginDialog(){
		setTitle("\u7BA1\u7406\u5458\u767B\u5F55");
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label = new JLabel("\u7528\u6237\u540D");
		label.setBounds(79, 63, 54, 15);
		contentPane.add(label);
		
		JLabel label_1 = new JLabel("\u5BC6\u7801");
		label_1.setBounds(79, 131, 54, 15);
		contentPane.add(label_1);
		
		textField = new JTextField();
		textField.setBounds(170, 60, 140, 21);
		contentPane.add(textField);
		textField.setColumns(10);
		
		password = new JPasswordField();
		password.setBounds(170, 128, 140, 21);
		contentPane.add(password);
		password.setColumns(10);
		
		JButton button = new JButton("\u767B\u5F55");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				applicationContext=new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
				UserMapper userMapper=(UserMapper) applicationContext.getBean("userMapper");
				User user;
				user =userMapper.findUserByName(textField.getText());
				
				String s=new String(password.getPassword()); 

				if(user.getPassword().equals(s)  ){
					new GameManage();
				}
				else{
					
					
					
					
				}
			}
		});
		Dimension screenSize=new Dimension();
		screenSize=Toolkit.getDefaultToolkit().getScreenSize();
		button.setBounds(170, 200, 93, 23);
		contentPane.add(button);
		setResizable(false);
	}
}
