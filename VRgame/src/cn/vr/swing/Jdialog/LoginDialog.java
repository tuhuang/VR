package cn.vr.swing.Jdialog;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.vr.mapper.UserMapper;
import cn.vr.po.User;

public class LoginDialog extends JDialog {
	private ApplicationContext applicationContext;
	
	
	private JPanel contentJpanel =new JPanel();
	private JLabel jl1=new JLabel("管理员登录");
	private JLabel jl2=new JLabel("账号");
	private JLabel jl3=new JLabel("密码");
	
	private JTextField jt=new JTextField(40);
	
	private JPasswordField jp=new JPasswordField(30);
	
	private JButton jb=new JButton("登录");
	
	private Box boxAll=Box.createVerticalBox();
	private Box box1=Box.createHorizontalBox();
	private Box box2=Box.createHorizontalBox();
	private Box box3=Box.createHorizontalBox();
	private Box box4=Box.createHorizontalBox();
	
	
	
	
	public LoginDialog() {
		
		// TODO Auto-generated constructor stub
		
		box1.add(jl1);
		
		box2.add(jl2);
		box2.add(jt);
		
		box3.add(jl3); 
		box3.add(jp);
		jb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				applicationContext=new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
				UserMapper userMapper = (UserMapper) applicationContext.getBean("userMapper");
				User user;
				
				
			}
		});
		
		box4.add(jb);
		
		boxAll.add(box1);
		boxAll.add(box2);
		boxAll.add(box3);
		boxAll.add(box4);
		contentJpanel.add(boxAll);
		getContentPane().add(contentJpanel);
		this.setVisible(true);
		
		
	}

}
