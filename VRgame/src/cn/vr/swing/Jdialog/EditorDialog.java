package cn.vr.swing.Jdialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.management.loading.PrivateClassLoader;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;

import org.aspectj.weaver.patterns.ThisOrTargetAnnotationPointcut;
import org.aspectj.weaver.patterns.IfPointcut.IfFalsePointcut;

import cn.vr.mapper.InformationMapper;
import cn.vr.other.Datasearch;
import cn.vr.po.Images;
import cn.vr.po.Information;
import cn.vr.swing.JCombox.Myjcombox;
import cn.vr.swing.JLable.MyimgLable;
import cn.vr.swing.Jpanel.Gamelist;
import cn.vr.swing.Jpanel.MybuttonEditor;
import cn.vr.swing.Jpanel.MybuttonRender;
import cn.vr.swing.Jtextarea.Mytextarea;

import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Insets;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.print.PrinterAbortException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
//游戏列表下点击编辑按钮跳出的额对话dialog
/*
 * 
 * author:黄龙
 * time:2016.8.1
 */
public class EditorDialog extends JDialog  implements ActionListener{

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			EditorDialog dialog = new EditorDialog(null,null,null);
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	// 定义图片面板
	private JPanel jPanel;

	private JLabel jLabel1;
	private JLabel jLabel2;
	 private JLabel jLabel3;
	private Myjcombox box;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JTextField field;
	private JTextField field2;
		
	
	private Mytextarea textArea;
	private JButton button;//确定修改
	private JButton addbt;//添加图片
    private JButton search;//添加路径
	private Vector<String> vec;
	Information information;
	Datasearch data = new Datasearch();
    private Gamelist gamelist;
    private JTable table;
    private Myjcombox myjcombox;
    
	public EditorDialog(Vector<String> v,JTable jTable,Myjcombox box) {
		
		this.table=jTable;
		this.myjcombox=box;
		initlocation();// 初始化大小及位置
       
		GridBagLayout gridBagLayout = new GridBagLayout();
		vec = v;
		initkongjian();// 初始化控件

		getContentPane().setLayout(gridBagLayout);

		GridBagConstraints constraints = new GridBagConstraints();
		// 第一行布局
		constraints.gridy = 0;
		constraints.gridx = 0;
		constraints.fill = constraints.NONE;
		constraints.weightx = 10;
		constraints.weighty = 10;
		add(jLabel1, constraints);

		constraints.gridy = 0;
		constraints.gridx = 1;
		constraints.fill = constraints.HORIZONTAL;
		constraints.weightx = 50;
		constraints.weighty = 10;
		add(field, constraints);

		constraints.gridy = 0;
		constraints.gridx = 2;
		constraints.fill = constraints.NONE;
		constraints.weightx = 10;
		constraints.weighty = 10;
		add(jLabel2, constraints);

		constraints.gridy = 0;
		constraints.gridx = 3;
		constraints.fill = constraints.NONE;
		constraints.weightx = 20;
		constraints.weighty = 10;
		constraints.anchor = constraints.WEST;
		add(box, constraints);

		// 第二行布局
		constraints.gridy = 1;
		constraints.gridx = 0;
		constraints.fill = constraints.NONE;
		constraints.weightx = 10;
		constraints.weighty = 10;
		constraints.anchor = constraints.NORTH;
		add(jLabel4, constraints);

		constraints.gridy = 1;
		constraints.gridx = 1;
		constraints.gridwidth = 2;
		constraints.weighty = 25;
		// constraints.gridheight=2;

		// constraints.weightx=80;
		//constraints.weighty = 20;
		constraints.fill = constraints.BOTH;
		JScrollPane jScrollPane = new JScrollPane(textArea);
		add(jScrollPane, constraints);

		// 第三行布局

		constraints.gridy = 2;
		constraints.gridx = 0;
		constraints.gridwidth = 1;
		constraints.weighty = 10;
		constraints.fill = constraints.NONE;
		add(jLabel5, constraints);

		
		constraints.gridy = 2;
		constraints.gridx = 1;
		constraints.gridwidth = 3;
		constraints.weighty = 45;
		constraints.fill = constraints.BOTH;
		JScrollPane scrollPane = new JScrollPane();

		initjpanel();// 初始化图片面板
		scrollPane.setViewportView(jPanel);// 为图片面板添加滚动条
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);// 隐藏垂直滚动
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);// 水平滚动
		add(scrollPane, constraints);
		// 第四行
		constraints.gridy = 3;
		constraints.gridx = 0;
		constraints.gridwidth = 1;
		constraints.weighty = 10;
		constraints.fill = constraints.NONE;
		constraints.anchor=constraints.SOUTH;
		add(jLabel3, constraints);
	    
		constraints.gridy = 3;
		constraints.gridx = 1;
		constraints.gridwidth = 2;
		constraints.weighty = 10;
		constraints.fill = constraints.HORIZONTAL;
		constraints.anchor=constraints.SOUTH;
		add(field2, constraints);
	
		constraints.gridy = 3;
		constraints.gridx = 3;
		constraints.gridwidth = 1;
		constraints.weighty = 10;
		constraints.fill = constraints.NONE;
		constraints.anchor=constraints.SOUTH;
		add(search, constraints);
	

		// 第5行布局
		constraints.gridy = 4;
		constraints.gridx = 3;
		constraints.gridwidth = 1;
		constraints.weighty = 10;
		constraints.fill = constraints.NONE;
		constraints.anchor = constraints.CENTER;
		add(button, constraints);

	}

	public void initlocation() {
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int width = (int) (d.getWidth() * 0.6);
		int height = (int) (d.getHeight() * 0.7);
		setBounds((d.width - width) / 2, (d.height - height) / 2, width, height);
	}

	// 控件初始化
	public void initkongjian() {

		jLabel1 = new JLabel("游戏名称");
		jLabel2 = new JLabel("游戏类型");
		jLabel3=new JLabel("游戏路径");
		box = new Myjcombox();
		jLabel4 = new JLabel("游戏简介");
		jLabel5 = new JLabel("图片展示");

		field = new JTextField();
		field2=new JTextField();
		textArea = new Mytextarea();
		button = new JButton("确定修改");
		search=new JButton("修改路径");
		button.addActionListener(this);
		search.addActionListener(this);
		information = data.SelectBynameandtype(vec.get(0), vec.get(1));// 根据名称和类型，存储查询到的游戏所有信息

		box.setSelectedItem(information.getType());
		field.setText(information.getName());
		field2.setText(information.getPath());
		textArea.setText(information.getIntroduce());
	}

	// 初始化图片面板
	public void initjpanel() {
		jPanel = new JPanel(new GridLayout(1, 0));

		addbt = new JButton("添加");
		List<String> list = data.Dataimages(information.getInfoId());
		int i;

		for (i = 0; i < list.size(); i++) {
			MyimgLable lable = new MyimgLable(list.get(i));
			lable.addMouseListener(new MymouseListener(lable, jPanel));
			jPanel.add(lable);
		}
		jPanel.add(addbt);
		addbt.addActionListener(this);

	}

	

	// 文件通道
	public void fileChannelCopy(File s, File t) {

		FileInputStream fi = null;

		FileOutputStream fo = null;

		FileChannel in = null;

		FileChannel out = null;

		try {

			fi = new FileInputStream(s);

			fo = new FileOutputStream(t);

			in = fi.getChannel();// 得到对应的文件通道

			out = fo.getChannel();// 得到对应的文件通道

			in.transferTo(0, in.size(), out);// 连接两个通道，并且从in通道读取，然后写入out通道

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				fi.close();

				in.close();

				fo.close();

				out.close();

			} catch (IOException e) {

				e.printStackTrace();
			}
		}
	}

	public class MymouseListener implements MouseListener {
		private MyimgLable myimgLable;
		private JPanel jPanel;

		public MymouseListener(MyimgLable myimgLable, JPanel jPanel) {
			super();
			this.myimgLable = myimgLable;
			this.jPanel = jPanel;
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			if (e.isMetaDown()) {
				int res;
				res = JOptionPane.showConfirmDialog(null, "确认删除？", "删除", JOptionPane.YES_NO_OPTION,
						JOptionPane.QUESTION_MESSAGE);
				if (res == JOptionPane.YES_OPTION) {
					if (!data.deltebyImgpath(myimgLable.getImgpath()))
						JOptionPane.showMessageDialog(null, "图片删除失败");
					else {
						//jPanel.remove(myimgLable);
						dispose();
						new EditorDialog(vec,table,box).show();
					}
				}
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
	}

	//监听按钮事件
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		//游戏信息修改操作
		if(e.getSource()==button)
		{
			Information inma=new Information();
			inma.setInfoId(information.getInfoId());
			inma.setIntroduce(textArea.getText());
			inma.setName(field.getText());
			inma.setPath(field2.getText());
			inma.setType(box.getSelectedItem().toString());
			int res;
			res = JOptionPane.showConfirmDialog(null, "确认修改", "修改", JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE);
			if (res == JOptionPane.YES_OPTION) 
			{
			   if(data.updateByinfoid(inma))
			   {
				   JOptionPane.showMessageDialog(null, "游戏信息修改成功");
				  Vector<String> colnames=new Vector<String>();
					colnames.add("名称");
					colnames.add("类型");
					colnames.add("路径");
					colnames.add("操作");
				   Datasearch data = new Datasearch();
				   DefaultTableModel	model = new DefaultTableModel(data.Dataname(box.getSelectedItem().toString()), colnames);
					table.setModel(model);
					table.getColumnModel().getColumn(3).setCellRenderer(new MybuttonRender());
					table.getColumnModel().getColumn(3).setCellEditor(new MybuttonEditor(table,box));
				  
			   }
			   else
				   JOptionPane.showMessageDialog(null, "游戏信息修改失败");
			}
			dispose();
			
		}
		//添加图片操作
		if(e.getSource()==addbt)
		{
			JFileChooser fileChooser = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter("图像文件(jpg/png/bmp)", "jpg", "png", "bmp");
			fileChooser.setFileFilter(filter);
			int i = fileChooser.showOpenDialog(getContentPane());
			Images image = new Images();
			if (i == JFileChooser.APPROVE_OPTION) {

				File selectedFile = fileChooser.getSelectedFile();
				selectedFile.getName();

				File old = new File(selectedFile.getPath());
				File news = new File(System.getProperty("user.dir") + "/pic/" + selectedFile.getName());
				fileChannelCopy(old, news);

				image.setInfoId(information.getInfoId());
				image.setPath("/pic/" + selectedFile.getName());

			}
			if (!data.insertimg(image))
				JOptionPane.showMessageDialog(null, "添加图片失败");
			else {
				JOptionPane.showMessageDialog(null, "添加图片成功");
				dispose();
				new EditorDialog(vec,table,box).show();
				;
			}
		}
		
		if(e.getSource()==search)
		{
			
			JFileChooser fileChooser=new JFileChooser();
			FileNameExtensionFilter filter=new FileNameExtensionFilter("游戏文件(exe)","exe");
			fileChooser.setFileFilter(filter);
			int i=fileChooser.showOpenDialog(getContentPane());
			if(i==JFileChooser.APPROVE_OPTION){
				
				File selectedFile=fileChooser.getSelectedFile();
				field2.setText(selectedFile.getPath());
				
			}
			
		}
	}
	
	

}
