package cn.vr.swing.Jdialog;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import cn.vr.other.Datasearch;
import cn.vr.po.Information;
import cn.vr.swing.JCombox.Myjcombox;
import cn.vr.swing.Jtextarea.Mytextarea;

import javax.swing.JLabel;
/*
 * author:黄龙
 * time:2016.8.1
 * 
 */
import javax.swing.JOptionPane;
/*
 * author:黄龙
 * time:2016.8.1
 */
//新增游戏JDialog
public class Addgame extends JDialog implements ActionListener {



	/**
	 * Launch the application.
	 */
	//定义控件
	private  JLabel jLabel1;
	private  JLabel jLabel2;
	private  JLabel jLabel3;
	private  JLabel jLabel4;
	private  JLabel jLabel5;
	private JTextField field1;
	private Myjcombox  box;
	private JTextField field2;
	private JTextField field3;
	private Mytextarea area;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	
	public static void main(String[] args) {
		try {
			Addgame dialog = new Addgame();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public Addgame() {
		setTitle("新增游戏");
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int width =(int)(d.getWidth()*0.4) ; 
		int height =(int)(d.getHeight()*0.8) ; 
		setBounds((d.width - width) / 2, (d.height - height) / 2, width, height);
		getContentPane().setLayout(new GridBagLayout());
	
		initkongjian();
		GridBagConstraints constraints=new GridBagConstraints();
		
		//第一行
		constraints.gridy=0;
		constraints.gridx=0;
		constraints.fill=constraints.NONE;
		constraints.weightx=20;
		constraints.weighty=10;
        add(jLabel1, constraints);
        
        constraints.gridy=0;
		constraints.gridx=1;
		constraints.fill=constraints.HORIZONTAL;
		constraints.weightx=40;
		constraints.weighty=10;
        add(field1, constraints);
	
		//第二行
        constraints.gridy=1;
		constraints.gridx=0;
		constraints.fill=constraints.NONE;
		constraints.weightx=20;
		constraints.weighty=10;
        add(jLabel2, constraints);
        
        constraints.gridy=1;
		constraints.gridx=1;
		constraints.fill=constraints.HORIZONTAL;
		constraints.weightx=40;
		constraints.weighty=10;
        add(box, constraints);
        
        //第三行
        constraints.gridy=2;
		constraints.gridx=0;
		constraints.fill=constraints.NONE;
		constraints.weightx=20;
		constraints.weighty=10;
        add(jLabel3, constraints);
        
        
        constraints.gridy=2;
		constraints.gridx=1;
		constraints.fill=constraints.BOTH;
		constraints.weightx=40;
		constraints.weighty=30;
		JScrollPane pane=new JScrollPane(area);
        add(pane, constraints);
	
		//第四行
        constraints.gridy=3;
		constraints.gridx=0;
		constraints.fill=constraints.NONE;
		constraints.weightx=20;
		constraints.weighty=10;
        add(jLabel4, constraints);
        
        constraints.gridy=3;
		constraints.gridx=1;
		constraints.fill=constraints.HORIZONTAL;
		constraints.weightx=50;
		constraints.weighty=10;
        add(field2, constraints);
        
        constraints.gridy=3;
		constraints.gridx=2;
		constraints.fill=constraints.NONE;
		constraints.weightx=30;
		constraints.weighty=10;
        add(button1, constraints);
        
        //第五行
        constraints.gridy=4;
		constraints.gridx=0;
		constraints.fill=constraints.NONE;
		constraints.weightx=20;
		constraints.weighty=10;
        add(jLabel5, constraints);
        
        constraints.gridy=4;
		constraints.gridx=1;
		constraints.fill=constraints.HORIZONTAL;
		constraints.weightx=50;
		constraints.weighty=10;
        add(field3, constraints);
        
        constraints.gridy=4;
		constraints.gridx=2;
		constraints.fill=constraints.NONE;
		constraints.weightx=30;
		constraints.weighty=10;
        add(button2, constraints);
        
        //第六行
        constraints.gridy=5;
		constraints.gridx=1;
		constraints.fill=constraints.NONE;
		constraints.ipadx=30;
		constraints.weighty=30;
        add(button3, constraints);
       
		
		
	}
	
	public void initkongjian()
	{
		jLabel1=new JLabel("游戏名称");
		jLabel2=new JLabel("游戏类型");
		jLabel3=new JLabel("游戏描述");
		jLabel4=new JLabel("展示图片");
		jLabel5=new JLabel("游戏链接");
		
		field1=new JTextField();
		field2=new JTextField();
		field3=new JTextField();
		box=new Myjcombox();
		
		area=new Mytextarea();
		
		button1=new JButton("上传图片");
		button2=new JButton("浏览路径");
		button3=new JButton("确认");
		
		button1.addActionListener(this);
		button2.addActionListener(this);
		button3.addActionListener(this);
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==button1)
		{
			JFileChooser fileChooser=new JFileChooser();
			FileNameExtensionFilter filter=new FileNameExtensionFilter("图像文件(jpg/png/bmp)","jpg","png","bmp");
			fileChooser.setFileFilter(filter);
			int i=fileChooser.showOpenDialog(getContentPane());
			
			if(i==JFileChooser.APPROVE_OPTION){
				
				File selectedFile=fileChooser.getSelectedFile();
				field2.setText(selectedFile.getName());
				
				File old=new File(selectedFile.getPath());
				 File news=new File(System.getProperty("user.dir")  +"/pic/"+selectedFile.getName());
				fileChannelCopy(old, news);
			}
			
			 
			
		}
		else if (e.getSource()==button2) {
			JFileChooser fileChooser=new JFileChooser();
			FileNameExtensionFilter filter=new FileNameExtensionFilter("游戏文件(exe)","exe");
			fileChooser.setFileFilter(filter);
			int i=fileChooser.showOpenDialog(getContentPane());
			if(i==JFileChooser.APPROVE_OPTION){
				
				File selectedFile=fileChooser.getSelectedFile();
				field3.setText(selectedFile.getPath());
				
			}
		}
		
		else if (e.getSource()==button3)
		{
			
			if(field1.getText().isEmpty())
			{
				JOptionPane.showMessageDialog(null, "名称不为空");
				return;
			}
			
			if(field2.getText().isEmpty())
			{
				JOptionPane.showMessageDialog(null, "图片不为空");
				return;
			}
			if(field3.getText().isEmpty())
			{
				JOptionPane.showMessageDialog(null, "游戏路径不为空");
				return;
			}
			Information information=new Information();
			
			information.setName(field1.getText());
			information.setPath(field3.getText());
			information.setType(box.getSelectedItem().toString());
			information.setIntroduce(area.getText());
			Datasearch datasearch=new Datasearch();
			String imgpath=  "/pic/"+field2.getText();
			if(!datasearch.insert(information, imgpath)){
				JOptionPane.showMessageDialog(null, "新增失败");
				
			}
			//setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
			dispose();
		}
		
		
	}
	
	//文件通道
	public void fileChannelCopy(File s, File t) {

        FileInputStream fi = null;

        FileOutputStream fo = null;

        FileChannel in = null;

        FileChannel out = null;

        try {

            fi = new FileInputStream(s);

            fo = new FileOutputStream(t);

            in = fi.getChannel();//得到对应的文件通道

            out = fo.getChannel();//得到对应的文件通道

            in.transferTo(0, in.size(), out);//连接两个通道，并且从in通道读取，然后写入out通道

        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                fi.close();

                in.close();

                fo.close();

                out.close();

            } catch (IOException e) {

                e.printStackTrace();

            }

        }

    }

	
}

