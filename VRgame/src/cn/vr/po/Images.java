package cn.vr.po;
/*
 * author:����
 * time:2016.7.29
 */
public class Images {
    private Integer imgId;

    private String path;

    private Integer infoId;

    
    public Images() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path == null ? null : path.trim();
    }

    public Integer getInfoId() {
        return infoId;
    }

    public void setInfoId(Integer infoId) {
        this.infoId = infoId;
    }
}