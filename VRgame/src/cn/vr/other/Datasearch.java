package cn.vr.other;

import java.util.ArrayList;
import java.util.Iterator;
/*
 * author:����
 * time:2016.7.29
 */
import java.util.List;
import java.util.Vector;

import org.aspectj.apache.bcel.generic.ReturnaddressType;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.vr.mapper.ImagesMapper;
import cn.vr.mapper.InformationMapper;
import cn.vr.po.Images;
import cn.vr.po.Information;
/*
 * 
 * author:黄龙
 * time:2016.8.1
 */
public class Datasearch {

	ClassPathXmlApplicationContext applicationContext;

	public Datasearch() {
		super();
		// TODO Auto-generated constructor stub
		applicationContext = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");

	}

	public Vector Dataname(String type) {

		InformationMapper information = (InformationMapper) applicationContext.getBean("informationMapper");

		Vector v2 = new Vector();
		List<Information> informations = information.selectByType(type);
		for (int j = 0; j < informations.size(); j++) {
			Vector v1 = new Vector();
			v1.add(informations.get(j).getName());
			v1.add(informations.get(j).getType());
			v1.add(informations.get(j).getPath());
			v2.add(v1);
		}
		return v2;

	}
//根据游戏信息表的id主键查询所有图片路径
	public List<String> Dataimages(int infoid) {
		ImagesMapper imagesMapper = (ImagesMapper) applicationContext.getBean("imagesMapper");
		List<Images> images = imagesMapper.selectByInfoid(infoid);
		List<String> paths = new ArrayList<>();
		for (int i = 0; i < images.size(); i++)
			paths.add(images.get(i).getPath());

		return paths;
	}
//插入游戏信息和图片路径
	public boolean insert(Information record, String path) {
		InformationMapper mapper1 = (InformationMapper) applicationContext.getBean("informationMapper");
		ImagesMapper mapper2 = (ImagesMapper) applicationContext.getBean("imagesMapper");
		if (mapper1.insert(record) == 0) {
			return false;

		}
		Images images = new Images();

		images.setInfoId(mapper1.returnId(record));
		images.setPath(path);
		if (mapper2.insert(images) == 0) {
			return false;
		}
		return true;
	}
	
	//根据游戏名称和类型查询该游戏完整信息
	public Information SelectBynameandtype(String name,String type){
		InformationMapper mapper1 = (InformationMapper) applicationContext.getBean("informationMapper");
		Information information=new Information();
		information.setName(name);
		information.setType(type);
		return mapper1.selectByPrimaryKey(mapper1.returnId(information));
	}
	
	//插入图片信息
	public boolean insertimg(Images images)
	{
		ImagesMapper mapper=(ImagesMapper)applicationContext.getBean("imagesMapper");
		if(mapper.insert(images)==0)
			return false;
		
		return true;
	}
public boolean deleteInfobyid(int id)
{
	InformationMapper mapper=(InformationMapper)applicationContext.getBean("informationMapper");
	ImagesMapper mapper1=(ImagesMapper)applicationContext.getBean("imagesMapper");
	if(mapper1.deleteByForeignKey(id)>0)
	{
	if(mapper.deleteByPrimaryKey(id)>0)
		return  true;
	else 
		return false;
	}
	else 
		return false;
}

//通过图片路径删除图片信息
public boolean deltebyImgpath(String path)
{
	ImagesMapper mapper1=(ImagesMapper)applicationContext.getBean("imagesMapper");
	if(mapper1.deleteBypath(path)==0)
		return false;
	
	return true;
}

public boolean updateByinfoid(Information information)
{
	InformationMapper mapper=(InformationMapper)applicationContext.getBean("informationMapper");
	if(mapper.updateByPrimaryKey(information)==0)
		return false;
	return true;
}

}
