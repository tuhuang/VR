package cn.vr.mapper;
/*
 * author:黄龙
 * time:2016.7.29
 */
import cn.vr.po.Information;
import cn.vr.po.InformationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface InformationMapper {
    int countByExample(InformationExample example);

    int deleteByExample(InformationExample example);

    int deleteByPrimaryKey(Integer infoId);

    int insert(Information record);

    int insertSelective(Information record);

    List<Information> selectByExample(InformationExample example);
    
    List<Information> selectByType(String type);

    Information selectByPrimaryKey(Integer infoId);
    //根据条件查询ID
     int returnId(Information record);

    int updateByExampleSelective(@Param("record") Information record, @Param("example") InformationExample example);

    int updateByExample(@Param("record") Information record, @Param("example") InformationExample example);

    int updateByPrimaryKeySelective(Information record);

    int updateByPrimaryKey(Information record);
}