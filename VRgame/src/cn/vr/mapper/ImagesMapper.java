package cn.vr.mapper;
/*
 * author:黄龙
 * time:2016.7.29
 */
import cn.vr.po.Images;
import cn.vr.po.ImagesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ImagesMapper {
    int countByExample(ImagesExample example);

    int deleteByExample(ImagesExample example);

    int deleteByPrimaryKey(Integer imgId);
    
    int deleteByForeignKey(Integer infoid);//根据外键删除
    
    int deleteBypath(String path);//根据外键删除

    int insert(Images record);

    int insertSelective(Images record);

    List<Images> selectByExample(ImagesExample example);
    
    List<Images> selectByInfoid(Integer infoid);
    
    Images selectByPrimaryKey(Integer imgId);

    int updateByExampleSelective(@Param("record") Images record, @Param("example") ImagesExample example);

    int updateByExample(@Param("record") Images record, @Param("example") ImagesExample example);

    int updateByPrimaryKeySelective(Images record);

    int updateByPrimaryKey(Images record);
}