package com.test;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicButtonUI;

import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import cn.vr.mapper.InformationMapper;
import cn.vr.po.Information;
import cn.vr.swing.Jdialog.newLoginDialog;



public class index extends JFrame {
	/*
	 * 整体的盒子框架的声明
	 */
	private ApplicationContext applicationContext;
	private JPanel contentPane;

	private Box boxAll = Box.createVerticalBox();

	private Box boxHead = Box.createHorizontalBox();

	private Box boxContent = Box.createHorizontalBox();

	private Box boxBottom = Box.createHorizontalBox();
	/**
	 * 内容区域的盒子
	 */
	private Box boxCon1 = Box.createVerticalBox();

	private Box boxCon2 = Box.createVerticalBox();

	private Box boxCon3 = Box.createVerticalBox();

	private Box boxCon4 = Box.createVerticalBox();

	/**
	 * 声明boxHead的内容
	 */
	private JLabel type1 = new JLabel("动作类");

	private JLabel type2 = new JLabel("冒险类");

	private JLabel type3 = new JLabel("策略类");

	private JLabel type4 = new JLabel("体育类");

	private JLabel type5 = new JLabel("竞速类");

	private JLabel type6 = new JLabel("射击类");

	private JLabel type7 = new JLabel("其他类");

	/**
	 * 声明boxHead中的详细内容
	 */
	
	
	
	private JLabel detail1_name = new JLabel();
	private JLabel detail1_content = new JLabel();
	private JLabel detail2_name = new JLabel();
	private JLabel detail2_content = new JLabel();
	private JLabel detail3_name = new JLabel();
	private JLabel detail3_content = new JLabel();
	private JLabel detail4_name = new JLabel();
	private JLabel detail4_content = new JLabel();
	
	private JButton jb1=new JButton("打开游戏");
	private JButton jb2=new JButton("打开游戏");
	private JButton jb3=new JButton("打开游戏");
	private JButton jb4=new JButton("打开游戏");
	//图片内容
	private JLabel jl1 =new JLabel();
	private JLabel jl2 =new JLabel();
	private JLabel jl3 =new JLabel();
	private JLabel jl4 =new JLabel();
	
	
	private Icon icon1;
	private Icon icon2;
	private Icon icon3;
	private Icon icon4;
	
	
	private JPanel jp;
	/*
	 * 左右侧边栏的控件
	 */
	private JButton jb_left= new JButton();
	private JButton jb_right=new JButton();

	/**
	 * 声明bottom的内容
	 */
	private JButton jb = new JButton("管理员登录");
	
	
	
	

	/**
	 * Create the frame.
	 */
	public index() {
		applicationContext = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
		InformationMapper informationMapper= (InformationMapper) applicationContext.getBean("informationMapper");
		String[] name=new String[4];
		String[] intro=new String[4];
		for(int i=0;i<4;i++){
			Information information=informationMapper.selectByPrimaryKey(i+1);
			name[i]=information.getName();
			intro[i]=information.getIntroduce();
		}
		
		Dimension screenSize =Toolkit.getDefaultToolkit().getScreenSize();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setBounds(0, 0, (int)screenSize.getWidth(),(int) screenSize.getHeight());
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		/**
		 * 头部信息
		 */
		
		
		getContentPane().add(boxHead,BorderLayout.NORTH);
		boxHead.add(Box.createHorizontalGlue());
		type1.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type1);
		boxHead.add(Box.createHorizontalGlue());
		type2.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type2);
		boxHead.add(Box.createHorizontalGlue());
		type3.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type3);
		boxHead.add(Box.createHorizontalGlue());
		type4.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type4);
		boxHead.add(Box.createHorizontalGlue());
		type5.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type5);
		boxHead.add(Box.createHorizontalGlue());
		type6.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type6);
		boxHead.add(Box.createHorizontalGlue());
		type7.setFont(new Font("楷体", Font.PLAIN, 25));
		boxHead.add(type7);
		
		/**
		 * 中间区域的布局
		 */
		getContentPane().add(boxContent, BorderLayout.CENTER);
		boxContent.add(Box.createHorizontalGlue());
		boxContent.add(boxCon1);
		boxContent.add(Box.createHorizontalGlue());
		boxContent.add(boxCon2);
		boxContent.add(Box.createHorizontalGlue());
		boxContent.add(boxCon3);
		boxContent.add(Box.createHorizontalGlue());
		boxContent.add(boxCon4);
		boxContent.add(Box.createHorizontalGlue());
		
		/**
		 * 中间区域的具体信息
		 */
//		
		
		//g.drawImage(img, 20, 20, 70, 100, boxCon1);
		//Icon icon=new ImageIcon(img);
		
		//image.setIcon(icon);
//		
		String url1="pic/newgame1.jpg";
		String url2="pic/newgame2.jpg";
		String url3="pic/newgame3.jpg";
		String url4="pic/newgame4.jpg";
		icon1=new ImageIcon(url1);
		icon2=new ImageIcon(url2);
		icon3=new ImageIcon(url3);
		icon4=new ImageIcon(url4);
		jl1.setIcon(icon1); 
		jl2.setIcon(icon2); 
		jl3.setIcon(icon3); 
		jl4.setIcon(icon4); 
		
		
		/**
		 * 给游戏加名字和简介
		 */
		
		detail1_name.setText(name[0]);
		detail1_content.setText(intro[0]);
		detail2_name.setText(name[1]);
		detail2_content.setText(intro[1]);
		detail3_name.setText(name[2]);
		detail3_content.setText(intro[2]);
		detail4_name.setText(name[3]);
		detail4_content.setText(intro[3]);
		
		boxCon1.add(jl1);
		boxCon1.add(detail1_name);
		boxCon1.add(detail1_content);
		jb1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				try {
					Runtime.getRuntime().exec("notepad.exe");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		
		boxCon1.add(jb1);
		boxCon2.add(jl2);
		boxCon2.add(detail2_name);
		boxCon2.add(detail2_content);
		boxCon2.add(jb2);
		boxCon3.add(jl3);
		boxCon3.add(detail3_name);
		boxCon3.add(detail3_content);
		boxCon3.add(jb3);
		boxCon4.add(jl4);
		boxCon4.add(detail4_name);
		boxCon4.add(detail4_content);
		boxCon4.add(jb4);
		//图片
		
		
		/*
		 * 左右两侧的控件
		 */
		
		String url11="pic/left.png";
		JButton jb_left = new JButton(null,new ImageIcon(url11));
		jb_left.setPreferredSize(new Dimension(80,200));
		jb_left.setUI(new BasicButtonUI());// 恢复基本视觉效果
		
		getContentPane().add(jb_left, BorderLayout.WEST);
		

		String url22="pic/right.png";
		JButton jb_right = new JButton(null,new ImageIcon(url22));
		jb_right.setPreferredSize(new Dimension(80,200));
		jb_right.setUI(new BasicButtonUI());// 恢复基本视觉效果
		getContentPane().add(jb_right, BorderLayout.EAST);
		
		
		
		
		/***
		 * 底部的布局
		 */
		getContentPane().add(boxBottom, BorderLayout.SOUTH);
		boxBottom.add(Box.createHorizontalGlue());
		jb.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//jp.createDialog(parentComponent, title)
				
				newLoginDialog ld=new newLoginDialog();
				ld.setLocation(300, 200);
				ld.setSize(500, 300);
				ld.show();
			}
		});
		
		boxBottom.add(jb);
		
		
		
		/**
		 * 背景图片的设定
		 */
		ImageIcon img = new ImageIcon("pic/bg.png");
		JLabel imgLabel = new JLabel(img); // JLabel imgLabel = new JLabel(new
											// ImageIcon("back.jpg"));
		getLayeredPane().add(imgLabel, new Integer(Integer.MIN_VALUE));
		imgLabel.setBounds(0, 0, img.getIconWidth(), img.getIconHeight()); // 背景图片的位置
		// 将contentPane设置成透明的
		((JPanel) getContentPane()).setOpaque(false);
		
		/**
		 * 字体颜色的设定
		 */
		type1.setForeground(Color.white);
		type2.setForeground(Color.white);
		type3.setForeground(Color.white);
		type4.setForeground(Color.white);
		type5.setForeground(Color.white);
		type6.setForeground(Color.white);
		type7.setForeground(Color.white);
		
		detail1_name.setForeground(Color.white);
		detail2_name.setForeground(Color.white);
		detail3_name.setForeground(Color.white);
		detail4_name.setForeground(Color.white);
		detail1_content.setForeground(Color.white);
		detail2_content.setForeground(Color.white);
		detail3_content.setForeground(Color.white);
		detail4_content.setForeground(Color.white);
		
		
	}
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BeautyEyeLNFHelper.launchBeautyEyeLNF();
					index frame = new index();
					frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
	}

}
