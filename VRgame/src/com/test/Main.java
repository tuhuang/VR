package com.test;
/**
 * author:黄龙
 * time:2016.7.26
 */
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JDesktopPane;
import javax.swing.JFileChooser;
import javax.swing.JButton;
import javax.swing.JTree;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.DropMode;

public class Main extends JFrame {
	private JLabel lblNewLabel;
	
	private JTextField gamepath;
	private JButton search;
	private JLabel lblNewLabel_2;
	private JLabel lblNewLabel_3;
	private JTextField gamename;
	private JButton add;
	private JScrollPane scrollPane;
	
	
	public Main() {
		super();
	
		getContentPane().setEnabled(false);
		setTitle("welcome");
		
		Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
		int width = 500;
		int height = 500;
		setBounds((d.width - width) / 2, (d.height - height) / 2, width, height);
		getContentPane().setBackground(new Color(255, 255, 255));
		getContentPane().setLayout(null);
		
	
		
		JLabel lblNewLabel_1 = new JLabel("游戏路径");
		lblNewLabel_1.setBounds(102, 136, 54, 15);
		getContentPane().add(lblNewLabel_1);
		
		gamepath = new JTextField();
		gamepath.setBounds(166, 133, 135, 21);
		getContentPane().add(gamepath);
		gamepath.setColumns(10);
		
		search = new JButton("浏览");
		search.addActionListener(new jbbutton());
		search.setBounds(329, 132, 93, 23);
		search.addActionListener(new jbbutton());
		getContentPane().add(search);
		
		lblNewLabel_2 = new JLabel("游戏名");
		lblNewLabel_2.setBounds(102, 100, 54, 15);
		getContentPane().add(lblNewLabel_2);
		
		lblNewLabel_3 = new JLabel("游戏简介");
		lblNewLabel_3.setBounds(102, 171, 54, 15);
		getContentPane().add(lblNewLabel_3);
		
		lblNewLabel = new JLabel("欢迎使用VR体验中心，祝您游戏愉快！");
		lblNewLabel.setBounds(126, 64, 256, 18);
		getContentPane().add(lblNewLabel);
		lblNewLabel.setFont(new Font("宋体", Font.PLAIN, 15));
		lblNewLabel.setToolTipText("");
		
		gamename = new JTextField();
		gamename.setBounds(166, 102, 135, 21);
		getContentPane().add(gamename);
		gamename.setColumns(10);
		
		add = new JButton("确认");
		add.setBounds(329, 348, 93, 23);
		add.addActionListener(new addbutton());
		getContentPane().add(add);
		
		scrollPane = new JScrollPane();
		scrollPane.setBounds(166, 175, 232, 147);
		getContentPane().add(scrollPane);
		
	
	}
	class jbbutton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			JFileChooser fileChooser=new JFileChooser();
			FileNameExtensionFilter filter=new FileNameExtensionFilter("游戏文件(EXE)","EXE");
			fileChooser.setFileFilter(filter);
			int i=fileChooser.showOpenDialog(getContentPane());
			if(i==JFileChooser.APPROVE_OPTION){
				
				File selectedFile=fileChooser.getSelectedFile();
				gamepath.setText(selectedFile.getPath());
				
			}
		}
		
	}
	class addbutton implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			String name=gamename.getText();
			String path=gamepath.getText();
			if(name.isEmpty())
			{
				JOptionPane.showMessageDialog(null, "游戏名不为空");
			return ;
			}
			
			if(path.isEmpty())
			{
				JOptionPane.showMessageDialog(null, "路径不为空");
			return ;
			}
			
		
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Main().setVisible(true);
			}
		});
	}
	

	public JLabel getLblNewLabel() {
		return lblNewLabel;
	}

	public JButton Button() {
		return search;
	}

	public JButton ADD() {
		return add;
	}

	
	public JScrollPane getScrollPane() {
		return scrollPane;
	}
}
